const toggle = () => {
    const element1 = document.getElementById("basic-price")
    const element2 = document.getElementById("professional-price")
    const element3 = document.getElementById("master-price")

    const yearlyBasic = '<span class="price" id="basic-price">\
        <span class="dollar">$</span>199.99\
    </span>'
    const monthlyBasic = '<span class="price" id="basic-price">\
        <span class="dollar">$</span>19.99\
    </span>'
    const yearlyProfessional = '<span class="price" id="professional-price">\
        <span class="dollar">$</span>249.99\
    </span>'
    const monthlyProfessional = '<span class="price" id="professional-price">\
        <span class="dollar">$</span>24.99\
    </span>'
    const yearlyMaster = '<span class="price" id="master-price">\
        <span class="dollar">$</span>399.99\
    </span>'
    const monthlyMaster = '<span class="price" id="master-price">\
        <span class="dollar">$</span>39.99\
    </span>'

    if (element1.innerHTML == monthlyBasic) {
        element1.innerHTML = yearlyBasic
    } else {
        element1.innerHTML = monthlyBasic
    }

    if (element2.innerHTML == monthlyProfessional) {
        element2.innerHTML = yearlyProfessional
    } else {
        element2.innerHTML = monthlyProfessional
    }

    if (element3.innerHTML == monthlyMaster) {
        element3.innerHTML = yearlyMaster
    } else {
        element3.innerHTML = monthlyMaster
    }
}